import RuleBase, {
  Rule,
  Condition,
  Value,
  ConditionImpl,
  ConstantValue,
  EqualsCondition,
  GreaterThanCondition,
  LessThanCondition,
  AndCondition,
  OrCondition,
  DateTimeValue,
  LogValue,
  RemoteValue,
  VariableValue
} from 'blackbox-rules'
import {serviceClass, autowired} from 'blackbox-ioc'

import {Service, NamedReference, hierarchical, nameOf} from 'blackbox-services'
import {DefaultRuleBase, RuleSerialiser, ConditionSerialisers, ValueSerialisers} from 'blackbox-rules-utils';

export default function init() {
  // nothing to do at this stage
  // but should be called for future use
  // importing init() will also ensure the service is loaded when the
  // blackboxService decorator is read

  new RuleSerialiser()
  new ConditionSerialisers()
  new ValueSerialisers()
}

interface Params {
  name?:string
  rule?:any
  condition?:any
  value?:any
}


//////////
// Setup rulebase hierarchy:
//
hierarchical({path:'/rulebase/rules'})(Rule)

const conditionHierarchy = hierarchical({path:'/rulebase/conditions'})
conditionHierarchy(ConditionImpl)
conditionHierarchy(EqualsCondition)
conditionHierarchy(GreaterThanCondition)
conditionHierarchy(LessThanCondition)
conditionHierarchy(AndCondition)
conditionHierarchy(OrCondition)

const valueHierarchy = hierarchical({path:'/rulebase/values'})
valueHierarchy(ConstantValue)
valueHierarchy(DateTimeValue)
valueHierarchy(LogValue)
valueHierarchy(RemoteValue)
valueHierarchy(VariableValue)
//
//////////

/**
 *
 */
@serviceClass('rulebase-service')
export class RulesService {
  @autowired('rulebase')
  rulebase:RuleBase

  constructor() {
    if(!this.rulebase)
      this.rulebase = new DefaultRuleBase()
  }

  getRulebaseService():Service {
    return {
      description: "Blackbox rule-base service.",
      bbversion: "1.0.0",
      links: {
        self: { href: "/" },
        rules: {
          href: "/rulebase/rules",
          description: "",
          types: [{
            uri:"http://ellipsistechnology.com/schemas/blackbox",
            name:"rule",
            format:"openapi:3.0:Schema Object"
          }]
        },
        conditions: {
          href: "/rulebase/conditions",
          description: "",
          types: [{
            uri:"http://ellipsistechnology.com/schemas/blackbox",
            name:"condition",
            format:"openapi:3.0:Schema Object"
          }]
        },
        values: {
          href: "/rulebase/values",
          description: "",
          types: [{
            uri:"http://ellipsistechnology.com/schemas/blackbox",
            name:"value",
            format:"openapi:3.0:Schema Object"
          }]
        }
      }
    }
  }

  getRules(_ps:Params={}):Rule[] {
    return this.rulebase.allRules()
  }

  createRule({rule}:Params):NamedReference { // TODO add created conditions and values to links when in verbose mode
    this.rulebase.addRule(rule)
    return nameOf(rule)
  }

  deleteRule({name}:Params) {
    this.rulebase.removeRule(name)
  }

  getRule({name}:Params):Rule {
    return this.rulebase.getRule(name)
  }

  updateRule({name, rule}:Params):NamedReference {
    const original = this.rulebase.getRule(name)
    this.rulebase.replaceRule(Object.assign(original, rule))
    return nameOf(rule)
  }

  replaceRule({name, rule}:Params):NamedReference {
    if(!rule.name){
      throw new Error(`Attempt to replace a rule without a name: ${JSON.stringify(rule)}`)
    }

    if(name !== rule.name) {
      this.rulebase.removeRule(name)
    }

    this.rulebase.replaceRule(rule)
    return nameOf(rule)
  }

  getConditions(_ps:Params):(Condition)[] {
    return this.rulebase.allConditions()
  }

  createCondition({condition}:Params):NamedReference {
    this.rulebase.addCondition(condition)
    return nameOf(condition)
  }

  deleteCondition({name}:Params) {
    this.rulebase.removeCondition(name)
  }

  getCondition({name}:Params):Condition {
    return this.rulebase.getCondition(name)
  }

  updateCondition({name, condition}:Params):NamedReference {
    const original = this.rulebase.getCondition(name)
    this.rulebase.replaceCondition(Object.assign(original, condition))
    return nameOf(condition)
  }

  replaceCondition({name, condition}:Params):NamedReference {
    if(!condition.name){
      throw new Error(`Attempt to replace a condition without a name: ${JSON.stringify(condition)}`)
    }

    if(name !== condition.name) {
      this.rulebase.removeCondition(name)
    }

    this.rulebase.replaceCondition(condition)
    return nameOf(condition)
  }

  getValues(_ps:Params):(Value<any>)[] {
    return this.rulebase.allValues()
  }
  createValue({value}:Params):NamedReference {
    this.rulebase.addValue(value)
    return nameOf(value)
  }
  deleteValue({name}:Params) {
    this.rulebase.removeValue(name)
  }
  getValue({name}:Params):Value<any> {
    return this.rulebase.getValue(name)
  }
  updateValue({name, value}:Params):NamedReference {
    const original = this.rulebase.getValue(name)
    this.rulebase.replaceValue(Object.assign(original, value))
    return nameOf(value)
  }
  replaceValue({name, value}:Params):NamedReference {
    if(!value.name){
      throw new Error(`Attempt to replace a condition without a name: ${JSON.stringify(value)}`)
    }

    if(name !== value.name) {
      this.rulebase.removeValue(name)
    }

    this.rulebase.replaceValue(value)
    return nameOf(value)
  }
}
