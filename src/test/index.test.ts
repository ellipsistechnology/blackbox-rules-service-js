import {RulesService} from '../index'
import RuleBase, { Rule, Condition, EqualsCondition, ValueImpl, ConditionImpl } from 'blackbox-rules';
import { factory, autowiredService, autowired } from 'blackbox-ioc';
import {DefaultRuleBase} from 'blackbox-rules-utils'
import { trimToDepth, verboseResponse, NamedReference, VerboseObject } from 'blackbox-services';

class RuleBaseFactory {
  @factory('rulebase')
  rulebase():RuleBase { return new DefaultRuleBase() }
}
class RuleBaseWrapper {
  @autowired('rulebase')
  rulebase:RuleBase
}
const rbWrapper = new RuleBaseWrapper()

beforeEach(() => {
  (rbWrapper.rulebase.store as any).rules = {};
  (rbWrapper.rulebase.store as any).conditions = {};
  (rbWrapper.rulebase.store as any).values = {};
})

test("RulesService is constructed with a RuleBase", () => {
  const rs = new RulesService()
  expect(rs.rulebase).toBeDefined()
  expect(rs.rulebase).toBeInstanceOf(RuleBase)
})

test("RulesService.getRulebaseService provides all service links", () => {
  const rs = new RulesService()
  const service = rs.getRulebaseService()

  expect(typeof service.description).toBe('string')
  expect(typeof service.bbversion).toBe('string')
  expect(service.links).toBeDefined()

  expect(service.links.self).toBeDefined()
  expect(typeof service.links.self.href).toBe('string')

  expect(service.links.rules).toBeDefined()
  expect(service.links.conditions).toBeDefined()
  expect(service.links.values).toBeDefined()
  expect(typeof service.links.rules.href).toBe('string')
  expect(typeof service.links.conditions.href).toBe('string')
  expect(typeof service.links.values.href).toBe('string')

  expect(service.links.rules.types).toBeDefined()
  expect(service.links.conditions.types).toBeDefined()
  expect(service.links.values.types).toBeDefined()

  expect(service.links.rules.types).toBeDefined()
  expect(service.links.conditions.types).toBeDefined()
  expect(service.links.values.types).toBeDefined()

  expect(service.links.rules.types.map).toBeInstanceOf(Function)
  expect(service.links.conditions.types.map).toBeInstanceOf(Function)
  expect(service.links.values.types.map).toBeInstanceOf(Function)

  expect(service.links.rules.types.length).toBeGreaterThan(0)
  expect(service.links.conditions.types.length).toBeGreaterThan(0)
  expect(service.links.values.types.length).toBeGreaterThan(0)

  service.links.rules.types.forEach( (type) => {
    expect(type.uri).toBeDefined
    expect(typeof type.uri).toBe('string')
  })
  service.links.conditions.types.forEach( (type) => {
    expect(type.uri).toBeDefined
    expect(typeof type.uri).toBe('string')
  })
  service.links.values.types.forEach( (type) => {
    expect(type.uri).toBeDefined
    expect(typeof type.uri).toBe('string')
  })
})

function makeTrigger(rb:RuleBase):Condition {
  return new EqualsCondition({
    left: rb.constantValue('val1').withName('const 1'),
    right: rb.constantValue('val1').withName('const 2'),
    name: 'test trigger'
  })
}

function makeAction(rb:RuleBase):Condition {
  let v = 'val3'
  return new EqualsCondition({
    left: {
      name: 'test value 3',
      type: 'test',
      get: async () => v,
      set: (val) => v = val
    },
    right: rb.constantValue('val4').withName('const 3'),
    name: 'test action'
  })
}

function makeRule(rb:RuleBase):Rule {
  return new Rule({
    name: 'test rule',
    trigger: makeTrigger(rb),
    action: makeAction(rb)
  })
}

test("Can get values, conditions and rule after creating a new rule", () => {
  const rs = new RulesService()
  const rule = makeRule(rs.rulebase)
  rs.createRule({rule:rule})

  // Check loading values:
  expect(rs.getValue({name:(rule.trigger as ConditionImpl).left.name}))
    .toBe((rule.trigger as ConditionImpl).left)
  expect(rs.getValue({name:(rule.trigger as ConditionImpl).right.name}))
    .toBe((rule.trigger as ConditionImpl).right)
  expect(rs.getValue({name:(rule.action as ConditionImpl).left.name}))
    .toBe((rule.action as ConditionImpl).left)
  expect(rs.getValue({name:(rule.action as ConditionImpl).right.name}))
    .toBe((rule.action as ConditionImpl).right)

  // Check loading conditions:
  expect(rs.getCondition({name:rule.trigger.name}))
    .toBe(rule.trigger)
  expect(rs.getCondition({name:rule.action.name}))
    .toBe(rule.action)

  // Check loading rule:
  expect(rs.getRule({name:rule.name})).toEqual(rule)
})


//////////
// Value CRUD
//////////

test("Can replace value", () => {
  const rs = new RulesService()
  const v1 = rs.rulebase.constantValue('val').withName('test')
  expect(rs.getValue({name:'test'})).toBe(v1)

  const v2 = new ValueImpl()
  v2.name = 'test'
  rs.replaceValue({name:'test', value:v2})
  expect(rs.getValue({name:'test'})).toBe(v2)
})

test("Can replace value with new name", () => {
  const rs = new RulesService()
  const v1 = rs.rulebase.constantValue('val').withName('test')
  expect(rs.getValue({name:'test'})).toBe(v1)

  const v2 = new ValueImpl()
  v2.name = 'test2'
  rs.replaceValue({name:'test', value:v2})
  expect(rs.getValue({name:'test'})).toBeUndefined()
  expect(rs.getValue({name:'test2'})).toBe(v2)
})

test("Can delete value", () => {
  const rs = new RulesService()
  const v1 = rs.rulebase.constantValue('val').withName('test')
  expect(rs.getValue({name:'test'})).toBe(v1)
  rs.deleteValue({name:v1.name})
  expect(rs.getValue({name:'test'})).toBeUndefined()
})

test("Can update value", () => {
  const rs = new RulesService()
  const v1 = rs.rulebase.constantValue('val').withName('test')
  expect(rs.getValue({name:'test'})).toBe(v1)
  expect(rs.getValue({name:'test'}).type).toEqual('constant')

  const v2 = {type: 't1'}
  rs.updateValue({name:'test', value:v2})
  expect(rs.getValue({name:'test'}).name).toEqual('test')
  expect(rs.getValue({name:'test'}).type).toEqual('t1')
})

test("Can get multiple values after adding", async () => {
  const rs = new RulesService()
  const vs = [1,2,3,4,5].map((i) => rs.rulebase.constantValue(`val${i}`).withName(`test${i}`))

  const response = rs.getValues({})
  expect(response.length).toBe(vs.length)

  await response.forEach( async (res, i) => {
    expect(res.name).toBe(vs[i].name)
    expect((res as any).val).toBe(await vs[i].get())
  })
})

test("Can't create value with same name as existing", () => {
  const rs = new RulesService()
  const v1 = rs.rulebase.constantValue('val').withName('test')
  expect(rs.getValue({name:'test'})).toBe(v1)

  const v2 = new ValueImpl()
  v2.name = 'test'
  expect(() => rs.createValue({value:v2})).toThrowError()
})


//////////
// Condition CRUD
//////////

test("Can replace condition", () => {
  const rs = new RulesService()
  const c1 = rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2'))
  c1.name = 'test'
  rs.rulebase.addCondition(c1)
  expect(rs.getCondition({name:'test'})).toBe(c1)

  const c2 = rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4'))
  c2.name = 'test'
  rs.replaceCondition({name:'test', condition:c2})
  expect(rs.getCondition({name:'test'})).toBe(c2)
})

test("Can replace condition with new name", () => {
  const rs = new RulesService()
  const c1 = rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2'))
  c1.name = 'test1'
  rs.rulebase.addCondition(c1)
  expect(rs.getCondition({name:'test1'})).toBe(c1)

  const c2 = rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4'))
  c2.name = 'test2'
  rs.replaceCondition({name:'test1', condition:c2})
  expect(rs.getCondition({name:'test1'})).toBeUndefined()
  expect(rs.getCondition({name:'test2'})).toBe(c2)
})

test("Can delete condition", () => {
  const rs = new RulesService()
  const c1 = rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2'))
  c1.name = 'test'
  rs.rulebase.addCondition(c1)
  expect(rs.getCondition({name:'test'})).toBe(c1)
  rs.deleteCondition({name:c1.name})
  expect(rs.getCondition({name:'test'})).toBeUndefined()
})

test("Can update condition", () => {
  const rs = new RulesService()
  const c1 = rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2'))
  c1.name = 'test'
  rs.rulebase.addCondition(c1)
  expect(rs.getCondition({name:'test'})).toBe(c1)

  const c2 = {type:'t1'}
  rs.updateCondition({name:'test', condition:c2})
  expect(rs.getCondition({name:'test'}).name).toEqual('test')
  expect(rs.getCondition({name:'test'}).type).toEqual('t1')
})

test("Can get multiple conditions after adding", () => {
  const rs = new RulesService()
  const cs = [1,2,3,4,5].map((i) => {
    const c = rs.rulebase.constantValue(`vala${i}`).withName(`testa${i}`)
      .eq(rs.rulebase.constantValue(`valb${i}`).withName(`testb${i}`))
      .withName(`test${i}`)
    return c
  })
  cs.forEach(c => rs.rulebase.addCondition(c))

  const response = rs.getConditions({})
  expect(response.length).toBe(cs.length)

  response.forEach( (res, i) => {
    expect(res.name).toBe(cs[i].name)
    expect((res as any).left).toBe((cs[i] as any).left)
    expect((res as any).right).toBe((cs[i] as any).right)
  })
})

test("Can't condition value with same name as existing", () => {
  const rs = new RulesService()
  const c1 = rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2'))
  c1.name = 'test'
  rs.rulebase.addCondition(c1)
  expect(rs.getCondition({name:'test'})).toBe(c1)

  const c2 = rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4'))
  c2.name = 'test'
  expect(() => rs.createCondition({condition:c2})).toThrowError()
})


//////////
// Rule CRUD
//////////

test("Can replace rule", () => {
  const rs = new RulesService()
  const r1 = rs.rulebase.when(
    rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2')).withName('trigger')
  ).then(
    rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4')).withName('action')
  ).withName('test')
  expect(rs.getRule({name:'test'})).toEqual(r1)

  const r2 = rs.rulebase.when(
    rs.rulebase.constantValue('val').withName('test5').eq(rs.rulebase.constantValue('val').withName('test6')).withName('trigger2')
  ).then(
    rs.rulebase.constantValue('val').withName('test7').eq(rs.rulebase.constantValue('val').withName('test8')).withName('action2')
  )
  r2.name = 'test'
  rs.replaceRule({name:'test', rule:r2})
  expect(rs.getRule({name:'test'})).toEqual(r2)
})

test("Can replace rule with new name", () => {
  const rs = new RulesService()
  const r1 = rs.rulebase.when(
    rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2')).withName('trigger')
  ).then(
    rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4')).withName('action')
  ).withName('test1')
  expect(rs.getRule({name:'test1'})).toEqual(r1)

  const r2 = rs.rulebase.when(
    rs.rulebase.constantValue('val').withName('test5').eq(rs.rulebase.constantValue('val').withName('test6')).withName('trigger2')
  ).then(
    rs.rulebase.constantValue('val').withName('test7').eq(rs.rulebase.constantValue('val').withName('test8')).withName('action2')
  )
  r2.name = 'test2'
  rs.replaceRule({name:'test1', rule:r2})
  expect(rs.getRule({name:'test1'})).toBeUndefined()
  expect(rs.getRule({name:'test2'})).toEqual(r2)
})

test("Can delete rule", () => {
  const rs = new RulesService()
  const r1 = rs.rulebase.when(
    rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2')).withName('trigger')
  ).then(
    rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4')).withName('action')
  ).withName('test1')
  expect(rs.getRule({name:'test1'})).toEqual(r1)

  rs.deleteRule({name:r1.name})
  expect(rs.getRule({name:'test1'})).toBeUndefined()
})

test("Can update rule", () => {
  const rs = new RulesService()
  const r1 = rs.rulebase.when(
    rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2')).withName('trigger')
  ).then(
    rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4')).withName('action')
  ).withName('test')
  expect(rs.getRule({name:'test'})).toEqual(r1)

  const r2 = {
    trigger:rs.rulebase.constantValue('val2').withName('test5')
      .eq(rs.rulebase.constantValue('val2').withName('test6'))
      .withName('trigger2')
  }
  rs.updateRule({name:'test', rule:r2})
  expect(rs.getRule({name:'test'}).name).toEqual('test')
  expect(rs.getRule({name:'test'}).trigger).toEqual(r2.trigger)
})

test("Can get multiple rules after adding", () => {
  const rs = new RulesService()
  const rules = [1,2,3,4,5].map((i) => {
    const r = rs.rulebase.when(
      rs.rulebase.constantValue(`vala${i}`).withName(`test1${i}`).eq(rs.rulebase.constantValue(`valb${i}`).withName(`test2${i}`)).withName(`trigger${i}`)
    ).then(
      rs.rulebase.constantValue(`valc${i}`).withName(`test3${i}`).eq(rs.rulebase.constantValue(`vald${i}`).withName(`test4${i}`)).withName(`action${i}`)
    ).withName(`test${i}`)
    return r
  })

  const response = rs.getRules({})
  expect(response.length).toBe(rules.length)

  response.forEach( (res, i) => {
    expect(res.name).toBe(rules[i].name)
    expect(res.trigger).toBe(rules[i].trigger)
    expect(res.action).toBe(rules[i].action)
  })
})

test("Can't create value with same name as existing", () => {
  const rs = new RulesService()
  const r1 = rs.rulebase.when(
    rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2')).withName('trigger')
  ).then(
    rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4')).withName('action')
  ).withName('test')
  expect(rs.getRule({name:'test'})).toEqual(r1)

  const r2 = rs.rulebase.when(
      rs.rulebase.constantValue('val').withName('test5').eq(rs.rulebase.constantValue('val').withName('test6')).withName('trigger2')
    ).then(
      rs.rulebase.constantValue('val').withName('test7').eq(rs.rulebase.constantValue('val').withName('test8')).withName('action2')
    )
  r2.name = 'test'
  expect(() => rs.createRule({rule:r2})).toThrowError()
})


//////////
// IOC
//////////

test("Can get instance of RulesService from IOC container", () => {
  class RulesServiceWrapper {
    @autowiredService('rulebase-service')
    service: any
  }

  const wrapper = new RulesServiceWrapper()
  expect(wrapper.service).toBeDefined()
  expect(wrapper.service.getRulebaseService).toBeDefined()
  expect(wrapper.service.getRulebaseService()).toBeDefined()
  expect(wrapper.service.getRulebaseService().description).toBe("Blackbox rule-base service.")
  expect(wrapper.service.getRules).toBeDefined()
  expect(wrapper.service.getRules()).toBeDefined()
})


//////////
// Verbose
//////////

test("Response after creating a new rule with verbose option includes verbose, named object", () => {
  const rs = new RulesService()
  const rule = makeRule(rs.rulebase)
  const response = <VerboseObject&NamedReference>verboseResponse(rs.createRule({rule:rule}), true)
  expect(response.name).toEqual(rule.name)
  expect(response.links).toBeDefined()
  expect(response.links.self).toBeDefined()
  expect(response.links.self.href).toBeDefined()
})


//////////
// Depth
//////////

test("getRule links to conditions and values based on depth.", () => {
  const rs = new RulesService()
  const r1 = rs.rulebase.when(
    rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2')).withName('trigger')
  ).then(
    rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4')).withName('action')
  ).withName('test')

  // Depth=undefined:
  const r2:any = trimToDepth(rs.getRule({name: 'test'}))
  expect(r2.name).toBe('test')

  expect(r2.trigger).toBeDefined()
  expect(r2.trigger.href).toBe('/rulebase/conditions/trigger')

  expect(r2.action).toBeDefined()
  expect(r2.action.href).toBe('/rulebase/conditions/action')

  // Depth=0:
  const r3:any = trimToDepth(rs.getRule({name: 'test'}), 0)
  expect(r3.name).toBe('test')

  expect(r3.trigger).toBeDefined()
  expect(r3.trigger.href).toBe('/rulebase/conditions/trigger')

  expect(r3.action).toBeDefined()
  expect(r3.action.href).toBe('/rulebase/conditions/action')

  // Depth=1:
  const r4:any = trimToDepth(rs.getRule({name: 'test'}), 1)
  expect(r4.name).toBe('test')

  expect(r4.trigger).toBeDefined()
  expect(r4.trigger.left).toBeDefined()
  expect(r4.trigger.left.href).toBe('/rulebase/values/test1')
  expect(r4.trigger.right).toBeDefined()
  expect(r4.trigger.right.href).toBe('/rulebase/values/test2')

  expect(r4.action).toBeDefined()
  expect(r4.action.left).toBeDefined()
  expect(r4.action.left.href).toBe('/rulebase/values/test3')
  expect(r4.action.right).toBeDefined()
  expect(r4.action.right.href).toBe('/rulebase/values/test4')

  // Depth=2:
  const r5 = trimToDepth(rs.getRule({name: 'test'}), 2)
  expect(r5).toEqual(r1)
})
