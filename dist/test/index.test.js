"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = require("../index");
var blackbox_rules_1 = require("blackbox-rules");
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_rules_utils_1 = require("blackbox-rules-utils");
var blackbox_services_1 = require("blackbox-services");
var RuleBaseFactory = /** @class */ (function () {
    function RuleBaseFactory() {
    }
    RuleBaseFactory.prototype.rulebase = function () { return new blackbox_rules_utils_1.DefaultRuleBase(); };
    __decorate([
        blackbox_ioc_1.factory('rulebase')
    ], RuleBaseFactory.prototype, "rulebase", null);
    return RuleBaseFactory;
}());
var RuleBaseWrapper = /** @class */ (function () {
    function RuleBaseWrapper() {
    }
    __decorate([
        blackbox_ioc_1.autowired('rulebase')
    ], RuleBaseWrapper.prototype, "rulebase", void 0);
    return RuleBaseWrapper;
}());
var rbWrapper = new RuleBaseWrapper();
beforeEach(function () {
    rbWrapper.rulebase.store.rules = {};
    rbWrapper.rulebase.store.conditions = {};
    rbWrapper.rulebase.store.values = {};
});
test("RulesService is constructed with a RuleBase", function () {
    var rs = new index_1.RulesService();
    expect(rs.rulebase).toBeDefined();
    expect(rs.rulebase).toBeInstanceOf(blackbox_rules_1.default);
});
test("RulesService.getRulebaseService provides all service links", function () {
    var rs = new index_1.RulesService();
    var service = rs.getRulebaseService();
    expect(typeof service.description).toBe('string');
    expect(typeof service.bbversion).toBe('string');
    expect(service.links).toBeDefined();
    expect(service.links.self).toBeDefined();
    expect(typeof service.links.self.href).toBe('string');
    expect(service.links.rules).toBeDefined();
    expect(service.links.conditions).toBeDefined();
    expect(service.links.values).toBeDefined();
    expect(typeof service.links.rules.href).toBe('string');
    expect(typeof service.links.conditions.href).toBe('string');
    expect(typeof service.links.values.href).toBe('string');
    expect(service.links.rules.types).toBeDefined();
    expect(service.links.conditions.types).toBeDefined();
    expect(service.links.values.types).toBeDefined();
    expect(service.links.rules.types).toBeDefined();
    expect(service.links.conditions.types).toBeDefined();
    expect(service.links.values.types).toBeDefined();
    expect(service.links.rules.types.map).toBeInstanceOf(Function);
    expect(service.links.conditions.types.map).toBeInstanceOf(Function);
    expect(service.links.values.types.map).toBeInstanceOf(Function);
    expect(service.links.rules.types.length).toBeGreaterThan(0);
    expect(service.links.conditions.types.length).toBeGreaterThan(0);
    expect(service.links.values.types.length).toBeGreaterThan(0);
    service.links.rules.types.forEach(function (type) {
        expect(type.uri).toBeDefined;
        expect(typeof type.uri).toBe('string');
    });
    service.links.conditions.types.forEach(function (type) {
        expect(type.uri).toBeDefined;
        expect(typeof type.uri).toBe('string');
    });
    service.links.values.types.forEach(function (type) {
        expect(type.uri).toBeDefined;
        expect(typeof type.uri).toBe('string');
    });
});
function makeTrigger(rb) {
    return new blackbox_rules_1.EqualsCondition({
        left: rb.constantValue('val1').withName('const 1'),
        right: rb.constantValue('val1').withName('const 2'),
        name: 'test trigger'
    });
}
function makeAction(rb) {
    var _this = this;
    var v = 'val3';
    return new blackbox_rules_1.EqualsCondition({
        left: {
            name: 'test value 3',
            type: 'test',
            get: function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                return [2 /*return*/, v];
            }); }); },
            set: function (val) { return v = val; }
        },
        right: rb.constantValue('val4').withName('const 3'),
        name: 'test action'
    });
}
function makeRule(rb) {
    return new blackbox_rules_1.Rule({
        name: 'test rule',
        trigger: makeTrigger(rb),
        action: makeAction(rb)
    });
}
test("Can get values, conditions and rule after creating a new rule", function () {
    var rs = new index_1.RulesService();
    var rule = makeRule(rs.rulebase);
    rs.createRule({ rule: rule });
    // Check loading values:
    expect(rs.getValue({ name: rule.trigger.left.name }))
        .toBe(rule.trigger.left);
    expect(rs.getValue({ name: rule.trigger.right.name }))
        .toBe(rule.trigger.right);
    expect(rs.getValue({ name: rule.action.left.name }))
        .toBe(rule.action.left);
    expect(rs.getValue({ name: rule.action.right.name }))
        .toBe(rule.action.right);
    // Check loading conditions:
    expect(rs.getCondition({ name: rule.trigger.name }))
        .toBe(rule.trigger);
    expect(rs.getCondition({ name: rule.action.name }))
        .toBe(rule.action);
    // Check loading rule:
    expect(rs.getRule({ name: rule.name })).toEqual(rule);
});
//////////
// Value CRUD
//////////
test("Can replace value", function () {
    var rs = new index_1.RulesService();
    var v1 = rs.rulebase.constantValue('val').withName('test');
    expect(rs.getValue({ name: 'test' })).toBe(v1);
    var v2 = new blackbox_rules_1.ValueImpl();
    v2.name = 'test';
    rs.replaceValue({ name: 'test', value: v2 });
    expect(rs.getValue({ name: 'test' })).toBe(v2);
});
test("Can replace value with new name", function () {
    var rs = new index_1.RulesService();
    var v1 = rs.rulebase.constantValue('val').withName('test');
    expect(rs.getValue({ name: 'test' })).toBe(v1);
    var v2 = new blackbox_rules_1.ValueImpl();
    v2.name = 'test2';
    rs.replaceValue({ name: 'test', value: v2 });
    expect(rs.getValue({ name: 'test' })).toBeUndefined();
    expect(rs.getValue({ name: 'test2' })).toBe(v2);
});
test("Can delete value", function () {
    var rs = new index_1.RulesService();
    var v1 = rs.rulebase.constantValue('val').withName('test');
    expect(rs.getValue({ name: 'test' })).toBe(v1);
    rs.deleteValue({ name: v1.name });
    expect(rs.getValue({ name: 'test' })).toBeUndefined();
});
test("Can update value", function () {
    var rs = new index_1.RulesService();
    var v1 = rs.rulebase.constantValue('val').withName('test');
    expect(rs.getValue({ name: 'test' })).toBe(v1);
    expect(rs.getValue({ name: 'test' }).type).toEqual('constant');
    var v2 = { type: 't1' };
    rs.updateValue({ name: 'test', value: v2 });
    expect(rs.getValue({ name: 'test' }).name).toEqual('test');
    expect(rs.getValue({ name: 'test' }).type).toEqual('t1');
});
test("Can get multiple values after adding", function () { return __awaiter(_this, void 0, void 0, function () {
    var rs, vs, response;
    var _this = this;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                rs = new index_1.RulesService();
                vs = [1, 2, 3, 4, 5].map(function (i) { return rs.rulebase.constantValue("val" + i).withName("test" + i); });
                response = rs.getValues({});
                expect(response.length).toBe(vs.length);
                return [4 /*yield*/, response.forEach(function (res, i) { return __awaiter(_this, void 0, void 0, function () {
                        var _a, _b;
                        return __generator(this, function (_c) {
                            switch (_c.label) {
                                case 0:
                                    expect(res.name).toBe(vs[i].name);
                                    _b = (_a = expect(res.val)).toBe;
                                    return [4 /*yield*/, vs[i].get()];
                                case 1:
                                    _b.apply(_a, [_c.sent()]);
                                    return [2 /*return*/];
                            }
                        });
                    }); })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
test("Can't create value with same name as existing", function () {
    var rs = new index_1.RulesService();
    var v1 = rs.rulebase.constantValue('val').withName('test');
    expect(rs.getValue({ name: 'test' })).toBe(v1);
    var v2 = new blackbox_rules_1.ValueImpl();
    v2.name = 'test';
    expect(function () { return rs.createValue({ value: v2 }); }).toThrowError();
});
//////////
// Condition CRUD
//////////
test("Can replace condition", function () {
    var rs = new index_1.RulesService();
    var c1 = rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2'));
    c1.name = 'test';
    rs.rulebase.addCondition(c1);
    expect(rs.getCondition({ name: 'test' })).toBe(c1);
    var c2 = rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4'));
    c2.name = 'test';
    rs.replaceCondition({ name: 'test', condition: c2 });
    expect(rs.getCondition({ name: 'test' })).toBe(c2);
});
test("Can replace condition with new name", function () {
    var rs = new index_1.RulesService();
    var c1 = rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2'));
    c1.name = 'test1';
    rs.rulebase.addCondition(c1);
    expect(rs.getCondition({ name: 'test1' })).toBe(c1);
    var c2 = rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4'));
    c2.name = 'test2';
    rs.replaceCondition({ name: 'test1', condition: c2 });
    expect(rs.getCondition({ name: 'test1' })).toBeUndefined();
    expect(rs.getCondition({ name: 'test2' })).toBe(c2);
});
test("Can delete condition", function () {
    var rs = new index_1.RulesService();
    var c1 = rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2'));
    c1.name = 'test';
    rs.rulebase.addCondition(c1);
    expect(rs.getCondition({ name: 'test' })).toBe(c1);
    rs.deleteCondition({ name: c1.name });
    expect(rs.getCondition({ name: 'test' })).toBeUndefined();
});
test("Can update condition", function () {
    var rs = new index_1.RulesService();
    var c1 = rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2'));
    c1.name = 'test';
    rs.rulebase.addCondition(c1);
    expect(rs.getCondition({ name: 'test' })).toBe(c1);
    var c2 = { type: 't1' };
    rs.updateCondition({ name: 'test', condition: c2 });
    expect(rs.getCondition({ name: 'test' }).name).toEqual('test');
    expect(rs.getCondition({ name: 'test' }).type).toEqual('t1');
});
test("Can get multiple conditions after adding", function () {
    var rs = new index_1.RulesService();
    var cs = [1, 2, 3, 4, 5].map(function (i) {
        var c = rs.rulebase.constantValue("vala" + i).withName("testa" + i)
            .eq(rs.rulebase.constantValue("valb" + i).withName("testb" + i))
            .withName("test" + i);
        return c;
    });
    cs.forEach(function (c) { return rs.rulebase.addCondition(c); });
    var response = rs.getConditions({});
    expect(response.length).toBe(cs.length);
    response.forEach(function (res, i) {
        expect(res.name).toBe(cs[i].name);
        expect(res.left).toBe(cs[i].left);
        expect(res.right).toBe(cs[i].right);
    });
});
test("Can't condition value with same name as existing", function () {
    var rs = new index_1.RulesService();
    var c1 = rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2'));
    c1.name = 'test';
    rs.rulebase.addCondition(c1);
    expect(rs.getCondition({ name: 'test' })).toBe(c1);
    var c2 = rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4'));
    c2.name = 'test';
    expect(function () { return rs.createCondition({ condition: c2 }); }).toThrowError();
});
//////////
// Rule CRUD
//////////
test("Can replace rule", function () {
    var rs = new index_1.RulesService();
    var r1 = rs.rulebase.when(rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2')).withName('trigger')).then(rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4')).withName('action')).withName('test');
    expect(rs.getRule({ name: 'test' })).toEqual(r1);
    var r2 = rs.rulebase.when(rs.rulebase.constantValue('val').withName('test5').eq(rs.rulebase.constantValue('val').withName('test6')).withName('trigger2')).then(rs.rulebase.constantValue('val').withName('test7').eq(rs.rulebase.constantValue('val').withName('test8')).withName('action2'));
    r2.name = 'test';
    rs.replaceRule({ name: 'test', rule: r2 });
    expect(rs.getRule({ name: 'test' })).toEqual(r2);
});
test("Can replace rule with new name", function () {
    var rs = new index_1.RulesService();
    var r1 = rs.rulebase.when(rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2')).withName('trigger')).then(rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4')).withName('action')).withName('test1');
    expect(rs.getRule({ name: 'test1' })).toEqual(r1);
    var r2 = rs.rulebase.when(rs.rulebase.constantValue('val').withName('test5').eq(rs.rulebase.constantValue('val').withName('test6')).withName('trigger2')).then(rs.rulebase.constantValue('val').withName('test7').eq(rs.rulebase.constantValue('val').withName('test8')).withName('action2'));
    r2.name = 'test2';
    rs.replaceRule({ name: 'test1', rule: r2 });
    expect(rs.getRule({ name: 'test1' })).toBeUndefined();
    expect(rs.getRule({ name: 'test2' })).toEqual(r2);
});
test("Can delete rule", function () {
    var rs = new index_1.RulesService();
    var r1 = rs.rulebase.when(rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2')).withName('trigger')).then(rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4')).withName('action')).withName('test1');
    expect(rs.getRule({ name: 'test1' })).toEqual(r1);
    rs.deleteRule({ name: r1.name });
    expect(rs.getRule({ name: 'test1' })).toBeUndefined();
});
test("Can update rule", function () {
    var rs = new index_1.RulesService();
    var r1 = rs.rulebase.when(rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2')).withName('trigger')).then(rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4')).withName('action')).withName('test');
    expect(rs.getRule({ name: 'test' })).toEqual(r1);
    var r2 = {
        trigger: rs.rulebase.constantValue('val2').withName('test5')
            .eq(rs.rulebase.constantValue('val2').withName('test6'))
            .withName('trigger2')
    };
    rs.updateRule({ name: 'test', rule: r2 });
    expect(rs.getRule({ name: 'test' }).name).toEqual('test');
    expect(rs.getRule({ name: 'test' }).trigger).toEqual(r2.trigger);
});
test("Can get multiple rules after adding", function () {
    var rs = new index_1.RulesService();
    var rules = [1, 2, 3, 4, 5].map(function (i) {
        var r = rs.rulebase.when(rs.rulebase.constantValue("vala" + i).withName("test1" + i).eq(rs.rulebase.constantValue("valb" + i).withName("test2" + i)).withName("trigger" + i)).then(rs.rulebase.constantValue("valc" + i).withName("test3" + i).eq(rs.rulebase.constantValue("vald" + i).withName("test4" + i)).withName("action" + i)).withName("test" + i);
        return r;
    });
    var response = rs.getRules({});
    expect(response.length).toBe(rules.length);
    response.forEach(function (res, i) {
        expect(res.name).toBe(rules[i].name);
        expect(res.trigger).toBe(rules[i].trigger);
        expect(res.action).toBe(rules[i].action);
    });
});
test("Can't create value with same name as existing", function () {
    var rs = new index_1.RulesService();
    var r1 = rs.rulebase.when(rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2')).withName('trigger')).then(rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4')).withName('action')).withName('test');
    expect(rs.getRule({ name: 'test' })).toEqual(r1);
    var r2 = rs.rulebase.when(rs.rulebase.constantValue('val').withName('test5').eq(rs.rulebase.constantValue('val').withName('test6')).withName('trigger2')).then(rs.rulebase.constantValue('val').withName('test7').eq(rs.rulebase.constantValue('val').withName('test8')).withName('action2'));
    r2.name = 'test';
    expect(function () { return rs.createRule({ rule: r2 }); }).toThrowError();
});
//////////
// IOC
//////////
test("Can get instance of RulesService from IOC container", function () {
    var RulesServiceWrapper = /** @class */ (function () {
        function RulesServiceWrapper() {
        }
        __decorate([
            blackbox_ioc_1.autowiredService('rulebase-service')
        ], RulesServiceWrapper.prototype, "service", void 0);
        return RulesServiceWrapper;
    }());
    var wrapper = new RulesServiceWrapper();
    expect(wrapper.service).toBeDefined();
    expect(wrapper.service.getRulebaseService).toBeDefined();
    expect(wrapper.service.getRulebaseService()).toBeDefined();
    expect(wrapper.service.getRulebaseService().description).toBe("Blackbox rule-base service.");
    expect(wrapper.service.getRules).toBeDefined();
    expect(wrapper.service.getRules()).toBeDefined();
});
//////////
// Verbose
//////////
test("Response after creating a new rule with verbose option includes verbose, named object", function () {
    var rs = new index_1.RulesService();
    var rule = makeRule(rs.rulebase);
    var response = blackbox_services_1.verboseResponse(rs.createRule({ rule: rule }), true);
    expect(response.name).toEqual(rule.name);
    expect(response.links).toBeDefined();
    expect(response.links.self).toBeDefined();
    expect(response.links.self.href).toBeDefined();
});
//////////
// Depth
//////////
test("getRule links to conditions and values based on depth.", function () {
    var rs = new index_1.RulesService();
    var r1 = rs.rulebase.when(rs.rulebase.constantValue('val').withName('test1').eq(rs.rulebase.constantValue('val').withName('test2')).withName('trigger')).then(rs.rulebase.constantValue('val').withName('test3').eq(rs.rulebase.constantValue('val').withName('test4')).withName('action')).withName('test');
    // Depth=undefined:
    var r2 = blackbox_services_1.trimToDepth(rs.getRule({ name: 'test' }));
    expect(r2.name).toBe('test');
    expect(r2.trigger).toBeDefined();
    expect(r2.trigger.href).toBe('/rulebase/conditions/trigger');
    expect(r2.action).toBeDefined();
    expect(r2.action.href).toBe('/rulebase/conditions/action');
    // Depth=0:
    var r3 = blackbox_services_1.trimToDepth(rs.getRule({ name: 'test' }), 0);
    expect(r3.name).toBe('test');
    expect(r3.trigger).toBeDefined();
    expect(r3.trigger.href).toBe('/rulebase/conditions/trigger');
    expect(r3.action).toBeDefined();
    expect(r3.action.href).toBe('/rulebase/conditions/action');
    // Depth=1:
    var r4 = blackbox_services_1.trimToDepth(rs.getRule({ name: 'test' }), 1);
    expect(r4.name).toBe('test');
    expect(r4.trigger).toBeDefined();
    expect(r4.trigger.left).toBeDefined();
    expect(r4.trigger.left.href).toBe('/rulebase/values/test1');
    expect(r4.trigger.right).toBeDefined();
    expect(r4.trigger.right.href).toBe('/rulebase/values/test2');
    expect(r4.action).toBeDefined();
    expect(r4.action.left).toBeDefined();
    expect(r4.action.left.href).toBe('/rulebase/values/test3');
    expect(r4.action.right).toBeDefined();
    expect(r4.action.right.href).toBe('/rulebase/values/test4');
    // Depth=2:
    var r5 = blackbox_services_1.trimToDepth(rs.getRule({ name: 'test' }), 2);
    expect(r5).toEqual(r1);
});
