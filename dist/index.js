"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_rules_1 = require("blackbox-rules");
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_services_1 = require("blackbox-services");
var blackbox_rules_utils_1 = require("blackbox-rules-utils");
function init() {
    // nothing to do at this stage
    // but should be called for future use
    // importing init() will also ensure the service is loaded when the
    // blackboxService decorator is read
    new blackbox_rules_utils_1.RuleSerialiser();
    new blackbox_rules_utils_1.ConditionSerialisers();
    new blackbox_rules_utils_1.ValueSerialisers();
}
exports.default = init;
//////////
// Setup rulebase hierarchy:
//
blackbox_services_1.hierarchical({ path: '/rulebase/rules' })(blackbox_rules_1.Rule);
var conditionHierarchy = blackbox_services_1.hierarchical({ path: '/rulebase/conditions' });
conditionHierarchy(blackbox_rules_1.ConditionImpl);
conditionHierarchy(blackbox_rules_1.EqualsCondition);
conditionHierarchy(blackbox_rules_1.GreaterThanCondition);
conditionHierarchy(blackbox_rules_1.LessThanCondition);
conditionHierarchy(blackbox_rules_1.AndCondition);
conditionHierarchy(blackbox_rules_1.OrCondition);
var valueHierarchy = blackbox_services_1.hierarchical({ path: '/rulebase/values' });
valueHierarchy(blackbox_rules_1.ConstantValue);
valueHierarchy(blackbox_rules_1.DateTimeValue);
valueHierarchy(blackbox_rules_1.LogValue);
valueHierarchy(blackbox_rules_1.RemoteValue);
valueHierarchy(blackbox_rules_1.VariableValue);
//
//////////
/**
 *
 */
var RulesService = /** @class */ (function () {
    function RulesService() {
        if (!this.rulebase)
            this.rulebase = new blackbox_rules_utils_1.DefaultRuleBase();
    }
    RulesService.prototype.getRulebaseService = function () {
        return {
            description: "Blackbox rule-base service.",
            bbversion: "1.0.0",
            links: {
                self: { href: "/" },
                rules: {
                    href: "/rulebase/rules",
                    description: "",
                    types: [{
                            uri: "http://ellipsistechnology.com/schemas/blackbox",
                            name: "rule",
                            format: "openapi:3.0:Schema Object"
                        }]
                },
                conditions: {
                    href: "/rulebase/conditions",
                    description: "",
                    types: [{
                            uri: "http://ellipsistechnology.com/schemas/blackbox",
                            name: "condition",
                            format: "openapi:3.0:Schema Object"
                        }]
                },
                values: {
                    href: "/rulebase/values",
                    description: "",
                    types: [{
                            uri: "http://ellipsistechnology.com/schemas/blackbox",
                            name: "value",
                            format: "openapi:3.0:Schema Object"
                        }]
                }
            }
        };
    };
    RulesService.prototype.getRules = function (_ps) {
        if (_ps === void 0) { _ps = {}; }
        return this.rulebase.allRules();
    };
    RulesService.prototype.createRule = function (_a) {
        var rule = _a.rule;
        this.rulebase.addRule(rule);
        return blackbox_services_1.nameOf(rule);
    };
    RulesService.prototype.deleteRule = function (_a) {
        var name = _a.name;
        this.rulebase.removeRule(name);
    };
    RulesService.prototype.getRule = function (_a) {
        var name = _a.name;
        return this.rulebase.getRule(name);
    };
    RulesService.prototype.updateRule = function (_a) {
        var name = _a.name, rule = _a.rule;
        var original = this.rulebase.getRule(name);
        this.rulebase.replaceRule(Object.assign(original, rule));
        return blackbox_services_1.nameOf(rule);
    };
    RulesService.prototype.replaceRule = function (_a) {
        var name = _a.name, rule = _a.rule;
        if (!rule.name) {
            throw new Error("Attempt to replace a rule without a name: " + JSON.stringify(rule));
        }
        if (name !== rule.name) {
            this.rulebase.removeRule(name);
        }
        this.rulebase.replaceRule(rule);
        return blackbox_services_1.nameOf(rule);
    };
    RulesService.prototype.getConditions = function (_ps) {
        return this.rulebase.allConditions();
    };
    RulesService.prototype.createCondition = function (_a) {
        var condition = _a.condition;
        this.rulebase.addCondition(condition);
        return blackbox_services_1.nameOf(condition);
    };
    RulesService.prototype.deleteCondition = function (_a) {
        var name = _a.name;
        this.rulebase.removeCondition(name);
    };
    RulesService.prototype.getCondition = function (_a) {
        var name = _a.name;
        return this.rulebase.getCondition(name);
    };
    RulesService.prototype.updateCondition = function (_a) {
        var name = _a.name, condition = _a.condition;
        var original = this.rulebase.getCondition(name);
        this.rulebase.replaceCondition(Object.assign(original, condition));
        return blackbox_services_1.nameOf(condition);
    };
    RulesService.prototype.replaceCondition = function (_a) {
        var name = _a.name, condition = _a.condition;
        if (!condition.name) {
            throw new Error("Attempt to replace a condition without a name: " + JSON.stringify(condition));
        }
        if (name !== condition.name) {
            this.rulebase.removeCondition(name);
        }
        this.rulebase.replaceCondition(condition);
        return blackbox_services_1.nameOf(condition);
    };
    RulesService.prototype.getValues = function (_ps) {
        return this.rulebase.allValues();
    };
    RulesService.prototype.createValue = function (_a) {
        var value = _a.value;
        this.rulebase.addValue(value);
        return blackbox_services_1.nameOf(value);
    };
    RulesService.prototype.deleteValue = function (_a) {
        var name = _a.name;
        this.rulebase.removeValue(name);
    };
    RulesService.prototype.getValue = function (_a) {
        var name = _a.name;
        return this.rulebase.getValue(name);
    };
    RulesService.prototype.updateValue = function (_a) {
        var name = _a.name, value = _a.value;
        var original = this.rulebase.getValue(name);
        this.rulebase.replaceValue(Object.assign(original, value));
        return blackbox_services_1.nameOf(value);
    };
    RulesService.prototype.replaceValue = function (_a) {
        var name = _a.name, value = _a.value;
        if (!value.name) {
            throw new Error("Attempt to replace a condition without a name: " + JSON.stringify(value));
        }
        if (name !== value.name) {
            this.rulebase.removeValue(name);
        }
        this.rulebase.replaceValue(value);
        return blackbox_services_1.nameOf(value);
    };
    __decorate([
        blackbox_ioc_1.autowired('rulebase')
    ], RulesService.prototype, "rulebase", void 0);
    RulesService = __decorate([
        blackbox_ioc_1.serviceClass('rulebase-service')
    ], RulesService);
    return RulesService;
}());
exports.RulesService = RulesService;
