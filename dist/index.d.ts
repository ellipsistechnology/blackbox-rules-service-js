import RuleBase, { Rule, Condition, Value } from 'blackbox-rules';
import { Service, NamedReference } from 'blackbox-services';
export default function init(): void;
interface Params {
    name?: string;
    rule?: any;
    condition?: any;
    value?: any;
}
/**
 *
 */
export declare class RulesService {
    rulebase: RuleBase;
    constructor();
    getRulebaseService(): Service;
    getRules(_ps?: Params): Rule[];
    createRule({ rule }: Params): NamedReference;
    deleteRule({ name }: Params): void;
    getRule({ name }: Params): Rule;
    updateRule({ name, rule }: Params): NamedReference;
    replaceRule({ name, rule }: Params): NamedReference;
    getConditions(_ps: Params): (Condition)[];
    createCondition({ condition }: Params): NamedReference;
    deleteCondition({ name }: Params): void;
    getCondition({ name }: Params): Condition;
    updateCondition({ name, condition }: Params): NamedReference;
    replaceCondition({ name, condition }: Params): NamedReference;
    getValues(_ps: Params): (Value<any>)[];
    createValue({ value }: Params): NamedReference;
    deleteValue({ name }: Params): void;
    getValue({ name }: Params): Value<any>;
    updateValue({ name, value }: Params): NamedReference;
    replaceValue({ name, value }: Params): NamedReference;
}
export {};
