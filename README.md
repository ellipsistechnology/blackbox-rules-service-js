# blackbox-rules-service

[![Version](https://img.shields.io/bitbucket/pipelines/ellipsistechnology/blackbox-rules-service-js.svg)](https://bitbucket.org/ellipsistechnology/blackbox-rules-service-js/addon/pipelines/home#!/)
[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

> A Blackbox service that utilises the blackbox-rules rulebase implementation.

Through the `init()` function, the rule service is automatically created for the Blackbox API rulebase. The services are added to the [Blackbox IOC](https://www.npmjs.com/package/blackbox-ioc) container for use by a Blackbox server (typically generated via the [Blackbox CLI](https://www.npmjs.com/package/blackbox-cli)).

For further information about the Blackbox Specification refer to the Blackbox website.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```
npm i blackbox-rules-service
```

## Usage

Load services into [Blackbox IOC](https://www.npmjs.com/package/blackbox-ioc) container:
```
import init from 'blackbox-rules-service'

init()
```
This will create a Blackbox service named 'rulebase-service' in the IOC container.
Note that serialisers and deserialisers from blackbox-rules-utils will also be loaded into the IOC container if not already loaded.

The 'rulebase-service' service depends on the 'rulebase' instance to already be loaded into the IOC container. A rulebase can be loaded as follows:
```
class RuleBaseFactory {
  @factory('rulebase')
  rulebase():RuleBase { return new DefaultRuleBase() }
}
```
See [blackbox-rules-utils](https://www.npmjs.com/package/blackbox-rules-utils) for instructions on creating a default RuleBase.

Generally the above two steps will be all you will want to do - for example, the rulebase is named via a factory and `init()` is called from index.ts in a Blackbox server generated with the [Blackbox CLI](https://www.npmjs.com/package/blackbox-cli). However, sometimes you may wish to access the rule service. This is easily done through autowiring:
```
class MyClass {
  @autowiredService('rule-service')
  ruleService:any
}
```

## Maintainers

[@ellipsistechnology](https://github.com/ellipsistechnology)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2019 Ben Millar
